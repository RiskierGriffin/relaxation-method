def relaxation_method2(func, xo, tol, max_it):
    count = 0
    list = []
    list.append(xo)
    x = xo
    if max_it == 1:
        return list
    else:
        while(count < max_it - 1):
            result = func(x)
            list.append(result)
            diff_num = ((result - x)**2)
            if len(list) < 3:
                diff_den = (2*x - result)
            elif len(list) >= 3:
                diff_den = (2*x - x_1 - result)
            if diff_den == 0:
                diff_den = 1e-14
            diff = (diff_num/diff_den)
            diff = abs(diff)
            if (diff < tol):
                print("Should break here")
                return list
            else:
                x_1 = x
                x = result
            count += 1
    return list
