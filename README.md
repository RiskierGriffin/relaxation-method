BASIC

A program that runs relaxation method on a function within a certain number of iterations

Performs the relaxation method to find a fixed-point for `func`,
given the initial guess `xo`. The relaxation process is carried out for
`num_it` steps.

Parameters
----------
func : Callable[[float], float]
The function whose fixed point is being found.
xo : float
The initial "guess" value.
num_it : int
The number of relaxation-iterations to perform.

Returns
-------
List[float]
A list of the initial guess, and all of the subsequent guesses generated
by the relaxation method.

ADVANCED

Performs the relaxation method to find a fixed-point for `func`,
given the initial guess `xo`. The relaxation process is carried out for
`num_it` steps.

Parameters
----------
func : Callable[[float], float]
The function whose fixed point is being found.
xo : float
The initial "guess" value.
tol : float
A positive value that sets the maximum permissable error
in the final fixed-point estimate.
max_it : int
The maximum number relaxation-guesses (i.e. the length of the
list you are creating) allotted before the
algorithm will end. The length of the list you return should
never exceed this number.

Returns
-------
List[float]
A list of the initial guess, and all of the subsequent guesses generated
by the relaxation method.
