#to test you need to def func(x)
def func(x):
    return x**2

def relaxation_method1(func, xo, num_int):
    list = []
    list.append(xo)
    if(num_int > 0):
        current_guess = 0
        current_guess = func(xo)
        list.append(current_guess)
        print(list)
        for current_iteration in range(1,num_int):
            previous_guess = current_guess
            current_guess = func(previous_guess)
            list.append(current_guess)
    return list
